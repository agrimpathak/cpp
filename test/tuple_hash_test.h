#pragma once

#include <gtest/gtest.h>
#include <functional>
#include <string>
#include <tuple>
#include <util/tuple_hash.h>


TEST(tuple_hash, tuple_hash)
{
    const int x         = 5;
    const double y      = -20.5;
    const std::string z = "abcdef";

    const size_t h_x = std::hash<int>()(x);
    const size_t h_y = std::hash<double>()(y);
    const size_t h_z = std::hash<std::string>()(z);

    const auto tup   = std::make_tuple(x, y, z);
    const size_t res = tuple_hash(tup);
    const size_t exp = h_x ^ h_y ^ h_z;

    ASSERT_EQ(res, exp);
}


TEST(tuple_hash, std_hash)
{
    const int x         = 5;
    const double y      = -20.5;
    const std::string z = "abcdef";
    const auto tup = std::make_tuple(x, y, z);
    const size_t res = std::hash<std::tuple<int, double, std::string>>()(tup);
    ASSERT_EQ(res, tuple_hash(tup));
}
