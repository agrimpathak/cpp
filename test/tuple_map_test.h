#pragma once

#include <gtest/gtest.h>
#include <util/tuple_map.h>


TEST(Tuplemap, end_to_end)
{
    TupleMap<int, int, char, int> map;
    map.emplace(5, 1, 'a', 3);
    map.emplace(7, 6, 'b', 6);
    map.emplace(8, 3, 'c', 1);

    ASSERT_EQ(map(6, 'b', 6), 7);
    ASSERT_EQ(map(3, 'c', 1), 8);
    ASSERT_EQ(map(1, 'a', 3), 5);

    ASSERT_EQ(map.contains(3, 'c', 1), true);
    ASSERT_EQ(map.contains(3, 'x', 1), false);
}
