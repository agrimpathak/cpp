#pragma once

#include <iostream>

/* === LOG LEVELS ===
 *  TRACE
 *  DEBUG
 *  INFO
 *  WARN
 *  ERROR
 *  FATAL
 */


#ifdef LOG_TRACE
#define LOG_DEBUG
#define LOG_INFO
#define LOG_WARN
#define LOG_ERROR
#define LOG_FATAL
#define TRACE(x) std::cout << x << "\n";
#define TRACE_NEWLINE() std::cout << "\n";
#else
#define TRACE(x)
#define TRACE_NEWLINE()
#endif


#ifdef LOG_DEBUG
#define LOG_INFO
#define LOG_WARN
#define LOG_ERROR
#define LOG_FATAL
#define DEBUG(x) std::cout << x << "\n";
#define DEBUG_NEWLINE() std::cout << "\n";
#else
#define DEBUG(x)
#define DEBUG_NEWLINE()
#endif


#ifdef LOG_INFO
#define LOG_WARN
#define LOG_ERROR
#define LOG_FATAL
#define INFO(x) std::cout << x << "\n";
#define INFO_NEWLINE() std::cout << "\n";
#else
#define INFO(x)
#define INFO_NEWLINE()
#endif


#ifdef LOG_WARN
#define LOG_ERROR
#define LOG_FATAL
#define WARN(x) std::cout << x << "\n";
#define WARN_NEWLINE() std::cout << "\n";
#else
#define WARN(x)
#define WARN_NEWLINE()
#endif


#ifdef LOG_ERROR
#define LOG_FATAL
#define ERROR(x) std::cerr << x << "\n";
#define ERROR_NEWLINE() std::cout << "\n";
#else
#define ERROR(x)
#define ERROR_NEWLINE()
#endif


#ifdef LOG_FATAL
#define FATAL(x) std::cerr << x << "\n";
#define FATAL_NEWLINE() std::cout << "\n";
#else
#define FATAL(x)
#define FATAL_NEWLINE()
#endif
