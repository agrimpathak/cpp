
template<typename Parent, typename K, typename V>
void KeyIterator<Parent, K, V>::init(const int key_cap, const int val_cap)
{
        Parent::init(key_cap, val_cap);
        key_vec_block.init(key_cap);
        key_vec    = key_vec_block.alloc_block(key_cap);
        key_vec_sz = 0;
}


template<typename Parent, typename K, typename V>
void KeyIterator<Parent, K, V>::init(const int key_cap)
{
        Parent::init(key_cap);
        key_vec_block.init(key_cap);
        key_vec    = key_vec_block.alloc_block(key_cap);
        key_vec_sz = 0;
}


template<typename Parent, typename K, typename V>
addr_t KeyIterator<Parent, K, V>::insert_key(const K& key)
{
        const addr_t kb_ad    = Parent::insert_key(key);
        key_vec[key_vec_sz++] = this->key_to_blk.access(kb_ad);
        return kb_ad;
}


template<typename Parent, typename K, typename V>
typename Parent::key_iterator_t
KeyIterator<Parent, K, V>::key_put_safely(const K& key, const V& val)
{
        if(!Parent::has_key(key))
        {
                insert_key(key);
        }
        Parent::key_put(key, val);
}

/*
 * key_put
 */
template<typename Parent, typename K, typename V>
struct KeyIterDelegate
{
        typedef typename Parent::key_iterator_t key_iterator_t;

        key_iterator_t key_put(KeyIterator<Parent, K, V>& caller,
                               const K& key,
                               const V& val,
                               key_iterator_t* const key_vec,
                               int& key_vec_sz)
        {
                return caller.key_put_parent(key, val);
        }
};


template<>
template<typename K, typename V>
struct KeyIterDelegate<ValueMap<K,V>, K, V>
{
        typedef typename ValueMap<K,V>::key_iterator_t key_iterator_t;

        key_iterator_t key_put(KeyIterator<ValueMap<K,V>, K, V>& caller,
                               const K& key,
                               const V& val,
                               key_iterator_t* const key_vec,
                               int& key_vec_sz)
        {
                key_iterator_t kv = caller.key_put_parent(key, val);
                key_vec[key_vec_sz++] = kv;
                return kv;
        }
};


template<typename Parent, typename K, typename V>
typename Parent::key_iterator_t
KeyIterator<Parent, K, V>::key_put(const K& key, const V& val)
{
        return KeyIterDelegate<Parent, K, V>{}
              .key_put(*this, key, val, key_vec, key_vec_sz);
}
