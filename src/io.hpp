
template<typename T>
void import_bin(T* const data,
                const size_t length,
                const char* const path)
{
        std::ifstream is(path, std::ios::binary | std::ios::in);
        if (!is.is_open())
        {
                return;
        }
        is.read(reinterpret_cast<char*>(data),
                std::streamsize(length * sizeof(T)) );
        is.close();
}


template<typename T>
void export_bin(const T* const data,
                const size_t length,
                const char* const path)
{
        std::ofstream os(path, std::ios::binary | std::ios::out);
        if (!os.is_open())
        {
                return;
        }
        os.write(reinterpret_cast<const char*>(data),
                 std::streamsize(length * sizeof(T)));
        os.close();
}


template<typename T>
void export_ascii(const T* const data,
                  const char* const path)
{
        std::ofstream os(path);
        if (!os.is_open())
        {
                return;
        }
        os << data;
        os.close();
}


template<typename T>
void export_matrix_csv(const T* const data,
                       const size_t m,
                       const size_t n,
                       const char* const path)
{
        std::ofstream file;
        file.open(path);
        file << std::setprecision(std::numeric_limits<double>::digits10 + 1);
        for (size_t i = 0; i < m; ++i)
        {
                for (size_t j = 0; j < n; ++j)
                {
                        file << data[n * i + j];
                        file << ",";
                }
                file << "\n";
        }
        file.close();
}


template<char DELIM>
bool parse_line(std::ifstream& file, std::vector<std::string>& vec)
{
        std::string line;
        if (!std::getline(file, line))
        {
                return false;
        }
        std::string value;
        std::stringstream line_ss(line);
        while(std::getline(line_ss, value, DELIM))
        {
                vec.push_back(value);
        }
        return true;
}


template<char DELIM>
FileContainer parse_file(const char* const path)
{
        FileContainer res;
        std::ifstream file(path, std::ios::in);

        __ASSERT__(file.is_open())

        std::vector<std::string> header, values;
        parse_line<DELIM>(file, header);
        const size_t width = header.size();
        values.reserve(width);

        while (parse_line<DELIM>(file, values))
        {
                FileRow r;
                r.reserve(width);
                for (size_t j = 0; j < width; ++j)
                {
                    r[header[j]] = values[j];
                }
                res.emplace_back(std::move(r));
                values.clear();
        }
        file.close();
        return res;
}
