
template<typename T>
SetIterator<T>::SetIterator(const T* const set_,
                            const int cap_,
                            T* const elems_,
                            int& elem_sz_) :
        set(set_),
        cap(cap_),
        elems(elems_),
        elem_sz(elem_sz_),
        pos(0)
{
}


template<typename T>
bool SetIterator<T>::has_next()
{
        while (pos < elem_sz)
        {
                const addr_t addr = set_locate(elems[pos], set, cap);
                if(elems[pos] == set[addr])
                {
                        return true;
                }
                arr_swp(elems, pos, --elem_sz);
        }
        return false;
}
