#ifndef LOGGER_H
#define LOGGER_H

#include <string>
#include "block.h"
#include "macro.h"

class Logger
{
public:
        Logger(const size_t mem_cap = 1 << 20)
        {
                mem.init(mem_cap);
                start = mem.alloc_block(mem_cap);
                data  = start;
                end   = start + mem_cap;
        }

        void log(const char* const input)
        {
                __ASSERT__(data <= end)
                strcpy(data, input);
                data += strlen(input);
        }


        void log(std::string&& str)
        {
                log(str.c_str());
        }

        void save(const char* const path)
        {
                data = '\0';
                export_ascii(start, path);
        }

private:
        Block<char> mem;

        char* data;

        char* start;

        char* end;
};

#endif // LOGGER_H
