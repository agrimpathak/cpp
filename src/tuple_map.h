#pragma once

#include <tuple>
#include <unordered_map>
#include "util/tuple_hash.h"


template<typename V, typename ...Args>
class TupleMap
{
public:

    typedef typename std::unordered_map<std::tuple<Args...>, V> Container;

    typedef Container::const_iterator const_iterator;

public:

    TupleMap() { }


    const_iterator emplace(const V& value, const Args&... key_args)
    {
        return emplace(value, std::make_tuple(key_args...));
    }


    const_iterator emplace(const V& value, const std::tuple<Args...>& key)
    {
        return _container.emplace(key, value).first;
    }


    const_iterator find(const Args&... args) const
    {
        return _container.find(std::make_tuple(args...));
    }


    const V& operator()(const Args&... args) const
    {
        return find(args...)->second;
    }


    bool contains(const Args&... args) const
    {
        return find(args...) != cend();
    }


    const_iterator cbegin() const
    {
        return _container.cbegin();
    }


    const_iterator cend() const
    {
        return _container.cend();
    }


    const_iterator begin() const
    {
        return cbegin();
    }


    const_iterator end() const
    {
        return cend();
    }


    size_t size() const
    {
        return _container.size();
    }

private:

    Container _container;
};
