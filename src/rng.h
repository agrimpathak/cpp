#ifndef RNG_H
#define RNG_H

#include <random>

template<typename DistributionType>
class Rng
{
public:
        template<typename ...Args>
        Rng(const size_t seed, Args&&... args) : gen(seed), dist(args...) { }

        Rng(Rng&)  = delete;
        Rng(Rng&&) = delete;
        Rng& operator=(Rng&)  = delete;
        Rng& operator=(Rng&&) = delete;

        typename DistributionType::result_type operator()()
        {
                return dist(gen);
        }

private:
        std::default_random_engine gen;

        DistributionType dist;
};


class UniformIntRng : public Rng<std::uniform_int_distribution<int>>
{
public:
        UniformIntRng(const int a,
                      const int b,
                      const size_t seed = 0) : Rng(seed, a, b) { }
};


class UniformRealRng : public Rng<std::uniform_real_distribution<double>>
{
public:
        UniformRealRng(const double a,
                       const double b,
                       const size_t seed = 0) : Rng(seed, a, b) { }
};


class UniformFloatRng : public Rng<std::uniform_real_distribution<float>>
{
public:
        UniformFloatRng(const float a,
                        const float b,
                        const size_t seed = 0) : Rng(seed, a, b) { }
};

#endif // RNG_H
