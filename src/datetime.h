#pragma once

#include <chrono>
#include <ctime>
#include <time.h>
#include <string>


inline
std::time_t parse_datetime(const std::string& dt_str, const char* format)
{
        std::tm tm = std::tm { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, nullptr};
        strptime(dt_str.c_str(), format, &tm);
        return timegm(&tm);
}


class DateTime
{
public:
        static constexpr const char* DEFAULT_FORMAT = "%Y-%m-%d %H:%M:%S";

public:
        static DateTime now()
        {
            return
                DateTime(
                        std::chrono::system_clock::to_time_t(
                            std::chrono::system_clock::now()
                            )
                        );
        }


        DateTime(
                const std::string& dt_str           ,
                const char* format = DEFAULT_FORMAT
                ) :
                _time(parse_datetime(dt_str, format))
        { }


        DateTime(const std::time_t time) :
                _time(time)
        { }


        DateTime(const DateTime& datetime) :
                _time(datetime._time)
        { }


        std::time_t epoch_time() const
        {
                return _time;
        }


        DateTime& set_time_of_day(
                const int hour       ,
                const int minute = 0 ,
                const int second = 0
                )
        {
                std::tm* tm = std::gmtime(&_time);
                tm->tm_hour = hour;
                tm->tm_min  = minute;
                tm->tm_sec  = second;
                _time = timegm(tm);
                return *this;
        }


        DateTime& to_next_day_of_week(const int wday)
        {
                std::tm* tm = std::gmtime(&_time);
                const int days_to_add = (
                        wday == tm->tm_wday ? 7 : (wday - tm->tm_wday + 7) % 7);
                tm->tm_mday += days_to_add;
                tm->tm_wday  = wday;
                tm->tm_hour  = 0;
                tm->tm_min   = 0;
                tm->tm_sec   = 0;
                _time = timegm(tm);
                return *this;
        }


        std::tm* to_struct() const
        {
                return std::gmtime(&_time);
        }


        char* asctime() const
        {
                return std::asctime(to_struct());
        }


        std::string to_string(const char* const format = DEFAULT_FORMAT) const
        {
                char buffer[32];
                std::strftime(buffer, 32, format, to_struct());
                return std::string(buffer);
        }


        DateTime& add_seconds(const std::time_t seconds)
        {
                _time += seconds;
                return *this;
        }


        DateTime& add_minutes(const std::time_t minutes)
        {
                add_seconds(60 * minutes);
                return *this;
        }


        DateTime& add_hours(const std::time_t hours)
        {
                add_seconds(3600 * hours);
                return *this;
        }


        DateTime& add_days(const std::time_t days)
        {
                add_seconds(86400 * days);
                return *this;
        }


        bool operator==(const DateTime& other) const
        {
                return _time == other._time;
        }


        bool operator<(const DateTime& other) const
        {
                return _time < other._time;
        }


        bool operator<=(const DateTime& other) const
        {
                return _time <= other._time;
        }


        bool operator>(const DateTime& other) const
        {
                return _time > other._time;
        }


        bool operator>=(const DateTime& other) const
        {
                return _time >= other._time;
        }

private:
        std::time_t _time;
};



namespace std
{
        template<>
        struct hash<DateTime>
        {
                auto operator()(const DateTime dt) const
                {
                        return hash<decltype(dt.epoch_time())>{}(dt.epoch_time());
                }
        };
}


namespace boost
{
        inline
        std::size_t hash_value(const DateTime dt)
        {
                return std::hash<DateTime>{}(dt);
        }
};
