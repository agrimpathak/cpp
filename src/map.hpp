
/*
 * AbstractBlockMap
 */
template<typename Derived, typename K, typename V>
addr_t AbstractBlockMap<Derived, K, V>::insert_key(const K& key)
{
        __ASSERT__(!has_key(key))
        const int blk_cap = key_cap * val_cap;
        addr_t blk_ad     = (hash<K>{}(key) % key_cap) * val_cap;
        addr_t ad         = blk_to_sz.locate_key(blk_ad);
        while(blk_to_sz.has_addr(ad))
        {
                blk_ad = (blk_ad + val_cap) % blk_cap;
                ad     = blk_to_sz.locate_key(blk_ad);
        }
        static_cast<Derived&>(*this).format_block(val_mem + blk_ad);
        blk_to_sz.to_set().force_insert(KeyValue<addr_t, int>{blk_ad, 0}, ad);
        return key_to_blk.to_set().insert(KeyValue<K, addr_t>{key, blk_ad});
}


template<typename Derived, typename K, typename V>
typename AbstractBlockMap<Derived, K, V>::key_iterator_t
AbstractBlockMap<Derived, K, V>::addr_put(const addr_t kb_ad, const V& val)
{
        __ASSERT__(0 <= kb_ad && kb_ad< key_cap)
        const KeyValue<K, addr_t>& kv = key_to_blk.access(kb_ad);
        const addr_t blk_ad = kv.val;
        int& blk_sz         = blk_to_sz.key_get(blk_ad);
        V* const blk        = val_mem + blk_ad;
        static_cast<Derived&>(*this).process_put(val, blk, blk_sz);
        return kv;
}


template<typename Derived, typename K, typename V>
typename AbstractBlockMap<Derived, K, V>::key_iterator_t
AbstractBlockMap<Derived, K, V>::key_put_safely(const K& key, const V& val)
{
        addr_t kb_ad = key_to_blk.locate(KeyValue<K,addr_t>{key});
        const KeyValue<K, addr_t>& kb_kv = key_to_blk.access(kb_ad);
        if(key != kb_kv.key)
        {
                kb_ad = insert_key(key);
        }
        return addr_put(kb_ad, val);
}


template<typename Derived, typename K, typename V>
void AbstractBlockMap<Derived, K, V>::del_key_safely(const K& key)
{
        const addr_t key_addr = key_to_blk.locate_key(key);
        if (key != key_to_blk.to_set().access(key_addr).key)
        {
                return;
        }
        const addr_t blk_ad = key_to_blk.addr_get(key_addr);
        key_to_blk.del_key(key);
        blk_to_sz.del_key(blk_ad);
}
