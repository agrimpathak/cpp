#ifndef PRIME_H
#define PRIME_H

#include <cmath>
#include "macro.h"

inline
void first_primes_continued(size_t* const primes,
                            const size_t sz,
                            size_t found)
{
        __ASSERT__(sz    >= 2)
        __ASSERT__(found >= 2)
        __ASSERT__(found <= sz)
        size_t cand = primes[found - 1] + 2;
        while(found < sz)
        {
                size_t sqrt_cand = static_cast<size_t>(sqrt(cand));
                for (size_t f = 1; primes[f] <= sqrt_cand; )
                {
                        if (cand % primes[f] == 0)
                        {
                                cand += 2;
                                f     = 1;
                                sqrt_cand = sqrt(cand);
                        } else {
                                ++f;
                        }
                }
                primes[found++] = cand;
                cand += 2;
        }
}


inline
void first_primes(size_t* const primes,
                  const size_t sz)
{
        __ASSERT__(sz >= 2)
        primes[0] = 2;
        primes[1] = 3;
        first_primes_continued(primes, sz, 2);
}


#endif
