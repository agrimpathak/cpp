#ifndef BLOCK_H
#define BLOCK_H

#include <cstddef>
#include <memory>
#include "macro.h"

template<typename T>
class Block
{
public:
        Block   () { }
        Block   (const Block&)          = delete;
        Block   (Block&&)               = delete;
        Block&  operator=(const Block&) = delete;
        Block&  operator=(Block&&)      = delete;

        inline
        void init(const size_t cap)
        {
                p0 = data.allocate(cap);
                p = p0;
                __DEBUG__(p_end = p0 + cap;)
        }


        inline
        const T& operator[](const size_t pos) const
        {
                return (const T&) *(p0 + pos);
        }


        inline
        T& operator[](const size_t pos)
        {
                return (T&) *(p0 + pos);
        }


        template<typename ...Args>
        inline
        T* alloc(Args&&... args)
        {
                __ASSERT__(p < p_end)
                data.construct(p, args...);
                return (T*) p++;
        }


        inline
        T* alloc_block(const size_t sz)
        {
                __ASSERT__(p + sz <= p_end)
                T* p_ = (T*) p;
                p += sz;
                return p_;
        }


        inline
        void clear()
        {
                p = p0;
        }


        inline
        T* begin()
        {
                return (T*) p0;
        }


        inline
        T* end()
        {
                return (T*) p;
        }


        inline
        const T* cbegin() const
        {
                return p0;
        }


        inline
        const T* cend() const
        {
                return p;
        }


        inline
        T* rbegin()
        {
                return (T*) (p - 1);
        }


        inline
        T* rend()
        {
                return p0 - 1;
        }


        inline
        const T* crbegin() const
        {
                return p - 1;
        }


        inline
        const T* crend() const
        {
                return p0 - 1;
        }


        inline
        size_t count() const
        {
                return p - p0;
        }

private:
        const T* p0;

        const T* p;

        __DEBUG__(const T* p_end;)

        std::allocator<T> data;

};

#endif // BLOCK_H
