#ifndef UTIL_H
#define UTIL_H

#include <algorithm>

template<typename T>
inline
void fill(T* const x, const int n, const T v)
{
	for (int i = 0; i < n; ++i)
	{
		x[i] = v;
	}
}


template<typename T>
inline
void arr_swp(T* const arr, const int x, const int y)
{
	std::swap(arr[x], arr[y]);
}


template<typename T>
inline
void swp(T& x, T& y)
{
	T tmp(std::move(x));
	x = std::move(y);
	y = std::move(tmp);
}

#endif // UTIL_H
