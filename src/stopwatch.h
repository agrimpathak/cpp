#ifndef STOPWATCH_H
#define STOPWATCH_H

#include <chrono>
#include <ctime>

class Stopwatch
{
public:
        inline
        void start()
        {
                start_wall = std::chrono::system_clock::now();
                start_cpu = clock();
        }


        inline
        size_t wall_time() const
        {
                return elapsed<std::chrono::seconds>();
        }


        inline
        clock_t cpu_time() const
        {
                return (clock() - start_cpu) / CLOCKS_PER_SEC;
        }


        template<typename Duration>
        inline
        size_t elapsed() const
        {
                const time_point end = std::chrono::system_clock::now();
                return std::chrono::duration_cast<Duration>(end - start_wall)
                      .count();
        }

private:
        typedef std::chrono::system_clock::time_point time_point;

        time_point start_wall;

        clock_t start_cpu;
};

#endif // STOPWATCH_H
