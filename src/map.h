#ifndef MAP_H
#define MAP_H

#include "block.h"
#include "macro.h"
#include "set.h"
#include "set_iter.h"
#include "util.h"

/*
 * KeyValue
 */
template<typename K, typename V>
struct KeyValue
{
        K key;
        V val;

        bool operator==(const KeyValue<K,V>& rhs) const
        {
                return key == rhs.key;
        }


        bool operator!=(const KeyValue<K,V>& rhs) const
        {
                return key != rhs.key;
        }
};


template<>
template<typename K, typename V>
struct SetCode<KeyValue<K, V>>
{
        static
        constexpr KeyValue<K, V> VACANT()
        {
                return KeyValue<K, V>{ SetCode<K>::VACANT() };
        }
};


template<>
template<typename K, typename V>
struct hash<KeyValue<K,V>>
{
        inline
        int operator()(const KeyValue<K,V>& elem) const
        {
                return hash<K>{}(elem.key);
        }
};


/*
 * ValueMap: Maps K -> V
 */
template<typename K, typename V>
class ValueMap
{
public:
        using key_iterator_t = KeyValue<K,V>;

        ValueMap  () {}
        ValueMap  (const ValueMap&)          = delete;
        ValueMap  (ValueMap&&)               = delete;
        ValueMap& operator=(const ValueMap&) = delete;
        ValueMap& operator=(ValueMap&&)      = delete;


        inline
        void init(const int cap)
        {
                __ASSERT__(cap > 0);
                set.init(cap);
                key_cap = cap;
        }


        inline
        const key_iterator_t& access(const addr_t addr) const
        {
                __ASSERT__(0 <= addr && addr < key_cap);
                return set.access(addr);
        }


        inline
        bool has_key(const K& key) const
        {
                return set.contains(KeyValue<K,V>{key});
        }


        inline
        bool has_addr(const addr_t addr) const
        {
                __ASSERT__(0 <= addr && addr < key_cap);
                return !set.is_vacant(addr);
        }


        inline
        addr_t locate_key(const K& key) const
        {
                return set.locate(KeyValue<K,V>{key});
        }


        inline
        key_iterator_t key_put(const K& key, const V& val)
        {
                const KeyValue<K,V> kv = KeyValue<K,V>{key, val};
                set.insert(kv);
                return kv;
        }


        inline
        V& key_get(const K& key) const
        {
                __ASSERT__(has_key(key))
                return addr_get(set.locate(KeyValue<K,V>{key}));
        }


        inline
        V& addr_get(const addr_t addr) const
        {
                __ASSERT__(0 <= addr && addr < key_cap)
                return set.access(addr).val;
        }


        inline
        void del_key(const K& key)
        {
                __ASSERT__(has_key(key))
                set.del_elem(KeyValue<K,V>{key});
        }


        inline
        void del_key_safely(const K& key)
        {
                set.del_elem_safely(KeyValue<K,V>{key});
        }


        inline
        void clear()
        {
                set.clear();
        }


        inline
        int count() const
        {
                return set.count();
        }


        inline
        Set<KeyValue<K,V>>& to_set()
        {
                return set;
        }


        inline
        KeyValue<K,V>* key_ptr() const
        {
                return set.data();
        }

protected:

        int key_cap;

private:

        Set<KeyValue<K,V>> set;
};



/*
 * AbstractBlockMap: Maps K -> Memory block of type V
 *
 * Note: Must call insert_key before key_put.  Alternatively, use key_put_safely
 */
template<typename Derived, typename K, typename V>
class AbstractBlockMap
{
protected:
        AbstractBlockMap () {}
public:
        using key_iterator_t = KeyValue<K, addr_t>;

        AbstractBlockMap (const AbstractBlockMap&)           = delete;
        AbstractBlockMap (AbstractBlockMap&&)                = delete;
        AbstractBlockMap& operator=(const AbstractBlockMap&) = delete;
        AbstractBlockMap& operator=(AbstractBlockMap&&)      = delete;


        inline
        void init(const int key_cap_, const int val_cap_)
        {
                __ASSERT__(key_cap_ > 0);
                __ASSERT__(val_cap_ > 0);
                key_cap = key_cap_;
                val_cap = val_cap_;
                key_to_blk.init(key_cap);
                blk_to_sz.init(key_cap);
                const int tot_val_sz = key_cap * val_cap;
                val_block.init(tot_val_sz);
                val_mem = val_block.alloc_block(tot_val_sz);
        }


        inline
        const KeyValue<K, addr_t>& access(const addr_t kb_ad) const
        {
                __ASSERT__(0 <= kb_ad && kb_ad < key_cap);
                return key_to_blk.access(kb_ad);
        }


        inline
        bool has_key(const K& key) const
        {
                return key_to_blk.has_key(key);
        }


        inline
        addr_t locate_key(const K& key) const
        {
                return key_to_blk.locate_key(key);
        }


        addr_t insert_key(const K& key);


        key_iterator_t addr_put(const addr_t kb_ad, const V& val);


        inline
        key_iterator_t key_put(const K& key, const V& val)
        {
                __ASSERT__(has_key(key))
                return addr_put(key_to_blk.locate_key(key), val);
        }


        key_iterator_t key_put_safely(const K& key, const V& val);


        inline
        void key_get(const K& key, V*& vals, int& sz) const
        {
                __ASSERT__(has_key(key))
                addr_get(key_to_blk.key_get(key), vals, sz);
        }


        inline
        V* key_get(const K& key) const
        {
                __ASSERT__(has_key(key))
                return addr_get(key_to_blk.key_get(key));
        }


        inline
        void addr_get(const addr_t blk_ad, V*& vals, int& sz) const
        {
                __ASSERT__(0 <= blk_ad && blk_ad < val_cap * key_cap);
                __ASSERT__(blk_ad % val_cap == 0)
                vals    = val_mem + blk_ad;
                sz      = blk_to_sz.key_get(blk_ad);
        }


        inline
        V* addr_get(const addr_t blk_ad) const
        {
                __ASSERT__(0 <= blk_ad && blk_ad < val_cap * key_cap);
                __ASSERT__(blk_ad % val_cap == 0)
                return val_mem + blk_ad;
        }


        inline
        addr_t get_block_addr(const K& key) const
        {
                __ASSERT__(has_key(key))
                return key_to_blk.key_get(key);
        }


        inline
        void del_key(const K& key)
        {
                __ASSERT__(has_key(key))
                const addr_t blk_ad = key_to_blk.key_get(key);
                key_to_blk.del_key(key);
                blk_to_sz.del_key(blk_ad);
        }


        void del_key_safely(const K& key);


        inline
        int key_count() const
        {
                __ASSERT__(key_to_blk.count() == blk_to_sz.count())
                return key_to_blk.count();
        }


        inline
        int& val_size(const K& key) const
        {
                return val_size_at_block_addr(key_to_blk.key_get(key));
        }


        inline
        int& val_size_at_block_addr(const addr_t blk_ad) const
        {
                __ASSERT__(0 <= blk_ad && blk_ad < val_cap * key_cap);
                __ASSERT__(blk_ad % val_cap == 0)
                return blk_to_sz.key_get(blk_ad);
        }


        inline
        void clear()
        {
                key_to_blk.clear();
                blk_to_sz.clear();
        }


        inline
        KeyValue<K, addr_t>* key_ptr()
        {
                return key_to_blk.to_set().data();
        }

protected:

        int key_cap;

        int val_cap;

        ValueMap<K, addr_t> key_to_blk;

        ValueMap<addr_t, int> blk_to_sz;

        V* val_mem;

private:

        Block<V> val_block;
};


/*
 * VectorMap: Maps K -> Vector of type V
 */
template<typename K, typename V>
class VectorMap;

template<typename K, typename V>
using VectorMapParent = AbstractBlockMap<VectorMap<K, V>, K, V>;


template<typename K, typename V>
class VectorMap: public VectorMapParent<K,V>
{
friend VectorMapParent<K, V>;
public:
        VectorMap () {}
        VectorMap (const VectorMap&)            = delete;
        VectorMap (VectorMap&&)                 = delete;
        VectorMap& operator=(const VectorMap&)  = delete;
        VectorMap& operator=(VectorMap&&)       = delete;

protected:

        void format_block(V* const blk) { }


        inline
        void process_put(const V& val, V* const blk, int& blk_sz)
        {
                blk[blk_sz++] = val;
        }
};


/*
 * SetMap: Masp K -> Set of type V
 */
template<typename K, typename V>
class SetMap;

template<typename K, typename V>
using SetMapParent = AbstractBlockMap<SetMap<K, V>, K, V>;

template<typename K, typename V>
class SetMap: public SetMapParent<K,V>
{
friend SetMapParent<K, V>;

public:
        SetMap () {}
        SetMap (const SetMap&)           = delete;
        SetMap (SetMap&&)                = delete;
        SetMap& operator=(const SetMap&) = delete;
        SetMap& operator=(SetMap&&)      = delete;


        inline
        void format_block(V* const blk)
        {
                fill(blk, this->val_cap, SetCode<V>::VACANT());
        }


        inline
        void del_key_val_soft(const K& key, const V& val)
        {
                del_block_val_soft(key, this->key_to_blk.key_get(key), val);
        }


        inline
        void del_block_val_soft(const K& key,
                                const addr_t blk_ad,
                                const V& val)
        {
                V* const blk            = this->val_mem + blk_ad;
                int& sz                 = this->blk_to_sz.key_get(blk_ad);
                set_delete_elem(val, blk, this->val_cap, sz);
        }


        inline
        void del_key_val_hard(const K& key, const V& val)
        {
                del_block_val_hard(key, this->key_to_blk.key_get(key), val);
        }


        inline
        void del_block_val_hard(const K& key, const addr_t blk_ad, const V& val)
        {
                V* const blk            = this->val_mem + blk_ad;
                int& sz                 = this->blk_to_sz.key_get(blk_ad);
                set_delete_elem(val, blk, this->val_cap, sz);
                if (sz == 0)
                {
                        this->key_to_blk.del_key(key);
                        this->blk_to_sz.del_key(blk_ad);
                }
        }

protected:

        inline
        void process_put(const V& val, V* const blk, int& blk_sz)
        {
                set_insert(val, blk, this->val_cap, blk_sz);
        }
};




/*
 * SetMapValIter: Same as SetMap, but allows iteration of values
 */
template<typename K, typename V>
class SetMapValIter: public SetMap<K, V>
{
public:
        inline
        void init(const int key_cap, const int val_cap)
        {
                SetMap<K,V>::init(key_cap, val_cap);
                const int sz    = key_cap * val_cap;
                val_vec_mem     .init(sz);
                val_vec_sz_mem  .init(this->key_cap);
                val_vec         = val_vec_mem.alloc_block(sz);
                val_vec_sz      = val_vec_sz_mem.alloc_block(this->key_cap);
                fill(val_vec_sz, this->key_cap, 0);
        }


        inline
        void key_put(const K& key, const V& val)
        {
                const KeyValue<K, addr_t>& kv = SetMap<K,V>::key_put(key, val);
                const int blk_ad        = kv.val;
                int* const vec          = val_vec + blk_ad;
                int& sz                 = val_vec_sz[blk_ad / this->val_cap];
                vec[sz++]               = val;
        }


        inline
        void del_key(const K& key)
        {
                const addr_t blk_ad = this->key_to_blk.key_get(key);
                val_vec_sz[blk_ad / this->val_cap] = 0;
                SetMap<K,V>::del_key(key);
        }


        inline
        void get_val_vec(const K& key, V*& vec, int& vec_sz) const
        {
                get_val_vec_for_block(  this->key_to_blk.key_get(key),
                                        vec, vec_sz);
        }


        inline
        V* get_val_vec(const K& key)
        {
                return get_val_vec_for_block(this->key_to_blk.key_get(key));
        }


        inline
        void get_val_vec_for_block(     const addr_t blk_ad,
                                        V*& vec,
                                        int& vec_sz) const
        {
                __ASSERT__(0 <= blk_ad)
                __ASSERT__(blk_ad < this->val_cap * this->key_cap)
                __ASSERT__(blk_ad % this->val_cap == 0)
                vec     = val_vec + blk_ad;
                vec_sz  = val_vec_sz[blk_ad / this->val_cap];
        }


        inline
        V* get_val_vec_for_block(const addr_t blk_ad) const
        {
                __ASSERT__(0 <= blk_ad)
                __ASSERT__(blk_ad < this->val_cap * this->key_cap)
                __ASSERT__(blk_ad % this->val_cap == 0)
                return val_vec + blk_ad;
        }


        inline
        SetIterator<V> value_iterator_for_key(const K& key) const
        {
                return
                value_iterator_for_block_ad(this->key_to_blk.key_get(key));
        }


        inline
        SetIterator<V> value_iterator_for_block_ad(const addr_t blk_ad) const
        {
                __ASSERT__(0 <= blk_ad)
                __ASSERT__(blk_ad < this->val_cap * this->key_cap)
                __ASSERT__(blk_ad % this->val_cap == 0)
                const V* const set      = this->val_mem + blk_ad;
                V* const vec            = val_vec + blk_ad;
                int& vec_sz             = val_vec_sz[blk_ad / this->val_cap];
                return SetIterator<V>(set, this->val_cap, vec, vec_sz);
        }


        inline
        void vacuum_key(const K& key)
        {
                vacuum_block(this->key_to_blk.key_get(key));
        }


        inline
        void vacuum_block(const addr_t blk_ad)
        {
                __ASSERT__(0 <= blk_ad)
                __ASSERT__(blk_ad < this->val_cap * this->key_cap)
                __ASSERT__(blk_ad % this->val_cap == 0)
                const V* const set = this->val_mem + blk_ad;
                V* const vec       = val_vec + blk_ad;
                int& vec_sz        = val_vec_sz[blk_ad / this->val_cap];
                set_vacuum(set, this->val_cap, vec, vec_sz);
        }

private:
        V* val_vec;

        int* val_vec_sz;

        Block<V> val_vec_mem;

        Block<int> val_vec_sz_mem;
};


#include "map.hpp"

#endif // MAP_H
