#ifndef IO_H
#define IO_H

#include <fstream>
#include <iomanip>
#include <ios>
#include <limits>
#include <sstream>
#include <string>
#include <sys/stat.h>
#include <unordered_map>
#include <vector>
#include "macro.h"


template<typename T>
void import_bin(T* const data,
                const size_t length,
                const char* const path);


template<typename T>
void export_bin(const T* const data,
                const size_t length,
                const char* const path);


template<typename T>
void export_ascii(const T* const data,
                  const char* const path);


template<typename T>
void export_matrix_csv(const T* const data,
                       const size_t m,
                       const size_t n,
                       const char* const path);


inline
bool file_exists(const char* const file_name)
{
        struct stat buffer;
        return stat(file_name, &buffer) == 0;
}


typedef std::unordered_map<std::string, std::string> FileRow;
typedef std::vector<FileRow> FileContainer;


template<char DELIM>
bool parse_line(std::ifstream& file, std::vector<std::string>& vec);


template<char DELIM>
FileContainer parse_file(const char* const path);


inline
FileContainer parse_csv(const char* const path)
{
        return parse_file<','>(path);
}


inline
FileContainer parse_tsv(const char* const path)
{
        return parse_file<'\t'>(path);
}


#include "io.hpp"


#endif // IO_H
