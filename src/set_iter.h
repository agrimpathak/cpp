#ifndef SET_ITER_H
#define SET_ITER_H

#include "set.h"

/* SetIterator
 *
 * Warnings:
 * 1) Iteration order is not guarenteed
 * 2) Deleted addresses are lazily removed has_next().
 */
template<typename T>
class SetIterator
{
public:
        SetIterator(const T* const set_,
                    const int cap_,
                    T* const elems_,
                    int& elems_sz_);


        bool has_next();


        inline
        T& next()
        {
                return elems[pos++];
        }


        inline
        void reset()
        {
                pos = 0;
        }


        inline
        void vacuum()
        {
                set_vacuum(set, cap, elems, elem_sz);
        }


        inline
        T* queue()
        {
                return elems;
        }


        inline
        int& queue_size()
        {
                return elem_sz;
        }

private:
        const T* const set;

        const int cap;

        T* const elems;

        int& elem_sz;

        int pos;
};

#include "set_iter.hpp"

#endif // SET_ITER_H
