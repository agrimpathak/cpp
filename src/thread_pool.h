#include <condition_variable>
#include <mutex>
#include <queue>
#include <thread>
#include <vector>


template<typename F>
class ThreadPool
{
public:

    ThreadPool(const int n_threads) :
        _n_threads(n_threads)
    {
    }


    ~ThreadPool()
    {
        finish();
    }


    void start()
    {
        init_threads();
    }


    void finish()
    {
        _terminate = true;
        _cv.notify_all();
        for (std::jthread& t : _threads)
        {
            if (t.joinable())
            {
                t.join();
            }
        }
    }


    void enqueue_task(F&& task)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        _tasks.push(task);
        lock.unlock();
        _cv.notify_all();
    }


    template<typename ...Args>
    void emplace_task(Args&&... args)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        _tasks.emplace(args...);
        lock.unlock();
        _cv.notify_all();
    }


    size_t size()
    {
        std::unique_lock<std::mutex> lock(_mutex);
        return _tasks.size();
    }


private:

    void thread_task()
    {
        while (true)
        {
            std::unique_lock<std::mutex> lock(_mutex);
            _cv.wait(lock, [&]() { return !_tasks.empty() || _terminate; });
            if (_terminate && _tasks.empty())
            {
                return;
            }
            F f = std::move(_tasks.front());
            _tasks.pop();
            lock.unlock();
            f();
        }
    }


    void init_threads()
    {
        _threads.reserve(_n_threads);
        for (int i = 0; i < _n_threads; ++i)
        {
            _threads.emplace_back(&ThreadPool<F>::thread_task, this);
        }
    }


private:

    std::condition_variable _cv;

    std::mutex _mutex;

    std::queue<F> _tasks;

    std::vector<std::jthread> _threads;

    bool _terminate = false;

    const int _n_threads;
};
