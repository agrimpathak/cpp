#ifndef KEY_ITER_H
#define KEY_ITER_H

#include "block.h"
#include "macro.h"
#include "map.h"
#include "set.h"
#include "util.h"

/*
 * KeyIterator
 *
 * "Attaches" methods and members that allows keys to be iterable to Maps
 */
template<typename Parent, typename K, typename V>
class KeyIterator;


template<typename K, typename V>
using ValueMapIter = KeyIterator<ValueMap<K, V>, K, V>;


template<typename K, typename V>
using VectorMapIter = KeyIterator<VectorMap<K, V>, K, V>;


template<typename K, typename V>
using SetMapIter = KeyIterator<SetMap<K, V>, K, V>;


template<typename K, typename V>
using SetMapDualIter = KeyIterator<SetMapValIter<K, V>, K, V>;


template<typename Parent, typename K, typename V>
class KeyIterator: public Parent
{
typedef typename Parent::key_iterator_t key_iterator_t;
public:
        KeyIterator  () { }
        KeyIterator  (const KeyIterator&)          = delete;
        KeyIterator  (KeyIterator&&)               = default;
        KeyIterator& operator=(const KeyIterator&) = delete;
        KeyIterator& operator=(KeyIterator&&)      = default;


        //Hack for ValueMap
        void init(const int key_cap);


        void init(const int key_cap, const int val_cap);


        addr_t insert_key(const K& key);


        key_iterator_t key_put(const K& key, const V& val);


        inline
        key_iterator_t key_put_parent(const K& key, const V& val)
        {
                return Parent::key_put(key, val);
        }


        key_iterator_t key_put_safely(const K& key, const V& val);


        inline
        SetIterator<key_iterator_t> key_iterator()
        {
                return SetIterator<key_iterator_t>(this->key_ptr(),
                                                   this->key_cap,
                                                   key_vec, key_vec_sz);
        }


        inline
        void clear()
        {
                Parent::clear();
                key_vec_sz = 0;
        }

private:
        key_iterator_t* key_vec;

        int key_vec_sz;

        Block<key_iterator_t> key_vec_block;
};

#include "key_iter.hpp"

#endif // KEY_ITER_H
