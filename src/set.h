#ifndef SET_H
#define SET_H

#include "macro.h"
#include "block.h"
#include "util.h"

typedef int addr_t;

/*
 * SetCodes
 */
template<typename T>
struct SetCode
{
        static constexpr T VACANT();
};


template<>
struct SetCode<int>
{
        static constexpr int VACANT() { return -1; }
};


/*
 * Hash
 */
template<typename T>
struct hash
{
        inline
        int operator()(const T& elem);
};


template<>
struct hash<int>
{
        inline
        int operator()(const int& elem) { return elem; }
};


/*
 * Set functions
 */
template<typename T>
addr_t set_locate(const T& elem,
                  const T* const set,
                  const int cap);


template<typename T>
inline
bool set_contains(const T& elem,
                  const T* const set,
                  const int cap)
{
        __ASSERT__(elem != SetCode<T>::VACANT())
        return elem == set[set_locate(elem, set, cap)];
}


template<typename T>
addr_t set_insert(const T& elem,
                  T* const set,
                  const int cap,
                  int& count);


template<typename T>
void set_delete_elem(const T& elem,
                     T* const set,
                     const int cap,
                     int& count);


template<typename T>
void set_delete_addr(const int addr,
                     T* const set,
                     const int cap,
                     int& count);


template<typename T>
void set_vacuum(const T* const set,
                const int cap,
                T* const elems,
                int& elem_sz);


/*
 * Set class
 */
template<typename E>
class Set
{
public:
        Set  () {}
        Set  (const Set&)          = delete;
        Set  (Set&&)               = delete;
        Set& operator=(const Set&) = delete;
        Set& operator=(Set&&)      = delete;


        inline
        void init(const int cap_)
        {
                __ASSERT__(cap_ > 0);
                mem.init(cap_);
                set = mem.alloc_block(cap_);
                cap = cap_;
                clear();
        }


        inline
        int insert(const E& elem)
        {
                return set_insert(elem, set, cap, count_);
        }


        inline
        void force_insert(const E& elem, const addr_t addr)
        {
                __ASSERT__(0 <= addr && addr < cap)
                __ASSERT__(set[addr] == SetCode<E>::VACANT())
                set[addr] = elem;
                ++count_;
        }


        inline
        bool is_vacant(const addr_t addr) const
        {
                return set[addr] == SetCode<E>::VACANT();
        }


        inline
        E& access(const addr_t addr) const
        {
                __ASSERT__(0 <= addr && addr < cap)
                return set[addr];
        }


        inline
        addr_t locate(const E& elem) const
        {
                return set_locate(elem, set, cap);
        }


        inline
        bool contains(const E& elem) const
        {
                return set_contains(elem, set, cap);
        }


        inline
        void del_elem(const E& elem)
        {
                __ASSERT__(set_contains(elem, set, cap))
                del_addr(locate(elem));
        }


        inline
        void del_addr(const addr_t addr)
        {
                __ASSERT__(0 <= addr && addr < cap)
                __ASSERT__(set[addr] != SetCode<E>::VACANT())
                __ASSERT__(count_ > 0)
                set_delete_addr(addr, set, cap, count_);
        }


        inline
        void del_elem_safely(const E& elem)
        {
                del_elem_safely(elem, locate(elem));
        }


        inline
        void del_elem_safely(const E& elem, const addr_t addr)
        {
                __ASSERT__(0 <= addr && addr < cap)
                if (elem == set[addr])
                {
                        set_delete_addr(addr, set, cap, count_);
                }
        }


        inline
        void clear()
        {
                count_ = 0;
                fill(set, cap, SetCode<E>::VACANT());
        }


        inline
        int count() const
        {
                return count_;
        }


        inline
        E* data() const
        {
                return set;
        }


protected:

        E* set;

        int cap;

        int count_;

        Block<E> mem;
};

#include "set.hpp"

#endif // SET_H
