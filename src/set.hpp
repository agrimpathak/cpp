#include "set.h"

template<typename T>
addr_t set_locate(const T& elem,
                  const T* const set,
                  const int cap)
{
        __ASSERT__(elem != SetCode<T>::VACANT())
        const int hsh = hash<T>{}(elem) % cap;
        addr_t addr   = hsh;
        while (elem != set[addr])
        {
                const T& tenant = set[addr];
                if (tenant == SetCode<T>::VACANT())
                {
                        return addr;
                }
                const int ten_hsh = hash<T>{}(tenant) % cap;
                const int ten_dis = (addr - ten_hsh + cap) % cap;
                const int elm_dis = (addr - hsh + cap) % cap;
                if (elm_dis > ten_dis)
                {
                        return addr;
                }
                addr = (addr + 1) % cap;
        }
        return addr;
}


template<typename T>
int set_insert(const T& elem,
               T* const set,
               const int cap,
               int& count)
{
        __ASSERT__(count <= cap);
        __ASSERT__(elem != SetCode<T>::VACANT())
        const addr_t addr = set_locate(elem, set, cap);
        if (elem == set[addr])
        {
                return addr;
        }
        const T tenant  = set[addr];
        set[addr]       = elem;
        if (tenant == SetCode<T>::VACANT())
        {
                ++count;
                return addr;
        }
        set_insert(tenant, set, cap, count);
        return addr;
}


template<typename T>
void set_delete_addr(const addr_t addr,
                     T* const set,
                     const int cap,
                     int& count)
{
        __ASSERT__(count <= cap);
        __ASSERT__(set[addr] != SetCode<T>::VACANT());
        set[addr] = SetCode<T>::VACANT();
        --count;
        int next_addr = (addr + 1) % cap;
        while (set[next_addr] != SetCode<T>::VACANT())
        {
                const int next_hsh = hash<T>{}(set[next_addr]) % cap;
                if (next_hsh == next_addr)
                {
                        return;
                }
                const T next_elem       = set[next_addr];
                set[next_addr]          = SetCode<T>::VACANT();
                --count;
                set_insert(next_elem, set, cap, count);
                next_addr = (next_addr + 1) % cap;
        }
}


template<typename T>
void set_delete_elem(const T& elem,
                     T* const set,
                     const int cap,
                     int& count)
{
        __ASSERT__(elem != SetCode<T>::VACANT())
        set_delete_addr(set_locate(elem, set, cap), set, cap, count);
}


template<typename T>
void set_vacuum(const T* const set,
                const int cap,
                T* const elems,
                int& elem_sz)
{
        __ASSERT__(elem_sz <= cap);
        for (int i = 0; i < elem_sz; )
        {
                if(!set_contains(elems[i], set, cap))
                {
                        arr_swp(elems, i, --elem_sz);
                        continue;
                }
                ++i;
        }
}
