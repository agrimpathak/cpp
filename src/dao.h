#ifndef DAO_H
#define DAO_H

#include <memory>
#include <string>
#include <pqxx/pqxx>

class Dao
{
public:
        Dao() { }


        void connect(const std::string& dbname                 ,
                     const std::string& user     = "postgres"  ,
                     const std::string& password = "postgres"  ,
                     const std::string& host     = "localhost" ,
                     const std::string& port     = "5432"      )
        {
                conn.reset(
                        new pqxx::connection(
                                std::string(" dbname   = ") + dbname   +
                                std::string(" user     = ") + user     +
                                std::string(" password = ") + password +
                                std::string(" host     = ") + host     +
                                std::string(" port     = ") + port
                        )
                );
        }


        void disconnect()
        {
                conn->disconnect();
        }


        pqxx::result query(const std::string& query_string) const
        {
                return pqxx::nontransaction(*conn).exec(query_string);
        }

private:
        std::unique_ptr<pqxx::connection> conn;
};


#endif // DAO_H
