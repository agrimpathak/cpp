#pragma once

#include <functional>
#include <tuple>
#include <type_traits>


template<typename T>
size_t hash_elem(const T& t)
{
    return std::hash<T>{}(t);
}


inline
size_t combine_hash(const size_t h1, const size_t h2)
{
    // Alternatively, boost::combine_hash
    return h1 ^ h2;
}


# pragma GCC diagnostic push
# pragma GCC diagnostic ignored "-Wunused-parameter"
template<
    size_t I,
    typename T,
    std::enable_if_t<(std::tuple_size_v<T> <= I), int> = 0
    >
size_t tuple_hash(size_t h, const T& tup)
{
    return h;
}
# pragma GCC diagnostic pop


template<
    size_t I,
    typename T,
    std::enable_if_t<(std::tuple_size_v<T> > I), int> = 0
    >
size_t tuple_hash(const size_t h, const T& tup)
{
    return tuple_hash<I + 1, T>(combine_hash(h, hash_elem(std::get<I>(tup))), tup);
}


template<typename T>
size_t tuple_hash(const T& tup)
{
    return tuple_hash<0, T>(0, tup);
}


namespace std
{
    template<typename ...Args>
    struct hash<std::tuple<Args...>>
    {
        size_t operator()(const std::tuple<Args...>& tup) const
        {
            return tuple_hash(tup);
        }
    };
}
